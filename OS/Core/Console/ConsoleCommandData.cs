﻿namespace OS.Core.Console
{
	/// <summary>
	/// Holds <see cref="ConsoleCommand"/> data during runtime.
	/// </summary>
	public struct ConsoleCommandData
	{
		public string Command;
		public string Info;
		public string Type;
		public string Method;
	}
}
