﻿using UnityEngine;
using Zenject;

namespace OS.Core.AddressableAssets 
{
    [CreateAssetMenu(fileName = "Inst_Assets_Module", menuName = CoreConfig.INSTALLERS_PATH + "Assets Module Installer")]
    public class AssetsModuleInstaller : ScriptableInstaller
    {
        protected override void HandleBindings()
        {
            Container.Bind<BaseAssetLinkFactory>().AsSingle().NonLazy();
            
            Container.Bind<IModule>().WithId(CoreConfig.INITIALIZE_ON_LOAD_ID).To<AssetsModule>().AsSingle().NonLazy();
            Container.Bind<IAssetsManager>().To<AssetsManager>().AsSingle().NonLazy();
        }
    }
}
