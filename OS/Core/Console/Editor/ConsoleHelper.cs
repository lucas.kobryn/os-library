﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using OS.Utilities;
using UnityEditor;
using UnityEngine;
using Logger = OS.Core.Logging.Logger;
using LogType = OS.Core.Logging.LogType;

namespace OS.Core.Console.Editor
{
	/// <summary>
	/// Utility class for <see cref="ConsoleModule"/>, bakes <see cref="ConsoleCommand"/>s in JSON format to a file.
	/// </summary>
	public static class ConsoleHelper
	{
		private static readonly Dictionary<string, MethodInfo> _commands = new Dictionary<string, MethodInfo>();
		private static readonly Dictionary<string, string> _infos = new Dictionary<string, string>();

		[MenuItem("Tools/Console/Bake Console Commands")]
		private static void BakeConsoleCommands()
		{
			_commands.Clear();
			_infos.Clear();

			List<Assembly> assemblies = new List<Assembly>();

			foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				if (assembly.GetName().Name.StartsWith("OS"))
				{
					assemblies.Add(assembly);
				}
			}

			foreach (Assembly assembly in assemblies)
			{
				Type[] tmpTypes = assembly.GetTypes();

				foreach (Type type in tmpTypes)
				{
					Debug.Log("Checking " + type.FullName);
					MethodInfo[] methods = type.GetMethods();

					foreach (MethodInfo method in methods)
					{
						object[] attributes = method.GetCustomAttributes(typeof(ConsoleCommand), false);

						foreach (Attribute attribute in attributes)
						{
							ConsoleCommand command = (ConsoleCommand) attribute;

							_commands.Add(command.Name, method);
							_infos.Add(command.Name, command.Info);

							break;
						}
					}
				}
			}

			EditorApplication.update += BuildCommandsScript;
		}

		private static void BuildCommandsScript()
		{
			EditorApplication.update -= BuildCommandsScript;
			
			StringBuilder builder = new StringBuilder();
			
			builder.AppendLine("[");

			int index = 0;
			foreach (var command in _commands)
			{
				_infos.TryGetValue(command.Key, out string info);
				
				builder.AppendLine("	{");
				builder.AppendLine($"		\"Command\": \"{command.Key.ToCamelCase()}\",");
				builder.AppendLine($"		\"Info\": \"{info ?? ""}\",");
				builder.AppendLine($"		\"Type\": \"{command.Value.ReflectedType.AssemblyQualifiedName}\",");
				builder.AppendLine($"		\"Method\": \"{command.Value.Name}\"");
				builder.Append("	}");
				
				if (index < _commands.Count - 1)
				{
					builder.Append(',');
					builder.AppendLine();
				}

				index++;
			}
			
			_commands.Clear();
			_infos.Clear();

			builder.AppendLine();
			builder.AppendLine("]");
			
			string filePath = $"{Application.dataPath}{ConsoleConfig.COMMANDS_PATH.Substring("Assets".Length)}{ConsoleConfig.COMMANDS_FILENAME}.{ConsoleConfig.COMMANDS_EXTENSION}";
			
			File.WriteAllText(filePath, builder.ToString(), Encoding.UTF8);
			AssetDatabase.ImportAsset($"{ConsoleConfig.COMMANDS_PATH}{ConsoleConfig.COMMANDS_FILENAME}.{ConsoleConfig.COMMANDS_EXTENSION}");
			
			Logger.Log("Finished baking Console Commands.", "Console", LogType.Info);
		}
	}
}
