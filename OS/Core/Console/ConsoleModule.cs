﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using OS.Utilities;
using UnityEngine;
using Zenject;
using Logger = OS.Core.Logging.Logger;

namespace OS.Core.Console
{
    /// <summary>
    /// Simple console that allows marking methods with <see cref="ConsoleCommand"/> attribute and invoking them during runtime via UI input.
    /// </summary>
    public class ConsoleModule : IModule, ITickable, IDisposable
    {
        public bool IsInitialized { get; private set; }
    
        public string Name => "Console Module";
        public int Priority => ModulePriorities.CONSOLE_MODULE;

        [Inject] private readonly ConsoleUI _consoleUI;
        [Inject] private readonly DiContainer _container;

        private ConsoleInputActions _input;
        private List<ConsoleCommandData> _data;
        
        private readonly Dictionary<string, MethodInfo> _commands = new Dictionary<string, MethodInfo>();
    
        public void Initialize()
        {
            _input = new ConsoleInputActions();
            _input.Enable();
            
            LoadCommands();
            
            _consoleUI.OnSubmit += TryReadCommand;

            if (ConsoleConfig.SHOW_DEBUG_LOGS)
            {
                Application.logMessageReceived += OutputDebugMessage;
            }

            IsInitialized = true;
        }
    
        public void Tick()
        {
            if (!IsInitialized)
            {
                return;
            }
        
            if (!_consoleUI.IsEnabled)
            {
                if (_input.Console.Open.triggered)
                {
                    _consoleUI.Toggle(true);
                }
                
                return;
            }

            if (_input.Console.Open.triggered || _input.Console.Close.triggered)
            {
                _consoleUI.Toggle(false);
            }
            else if (_input.Console.CycleHistoryUp.triggered)
            {
                _consoleUI.CycleHistory(-1);
            }
            else if (_input.Console.CycleHistoryDown.triggered)
            {
                _consoleUI.CycleHistory(1);
            }
        }

        public void CleanUp()
        {
            _input?.Dispose();

            if (ConsoleConfig.SHOW_DEBUG_LOGS)
            {
                Application.logMessageReceived -= OutputDebugMessage;
            }
            
            IsInitialized = false;
        }
        
        public void Dispose()
        {
            CleanUp();
        }

        [ConsoleCommand("Help", "Shows all available commands.")]
        public string Help()
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine("<#FFBC00>Available commands:</color>");

            int index = 0;
            foreach (ConsoleCommandData data in _data)
            {
                builder.Append(data.Command);
                builder.Append(": ");
                builder.Append("<#ABABAB>");
                builder.Append(data.Info);
                builder.Append("</color>");

                if (index < _data.Count - 1)
                {
                    builder.AppendLine();
                }

                index++;
            }

            return builder.ToString();
        }

        [ConsoleCommand("Clear", "Clears the console output.")]
        public void Clear()
        {
            _consoleUI.ClearOutput();
        }

        private void TryReadCommand(string command)
        {
            string[] split = command.Split(ConsoleConfig.COMMAND_PARAMETERS_DELIMITERS, StringSplitOptions.RemoveEmptyEntries);
            object[] parameters = new object[0];
            
            string commandName = split[0].ToCamelCase();

            if (split.Length > 1)
            {
                parameters = new object[split.Length - 1];

                for (int i = 1; i < split.Length; i++)
                {
                    parameters[i - 1] = split[i];
                }

                parameters = HandleParameters(parameters);
            }

            if (!_commands.TryGetValue(commandName, out MethodInfo method))
            {
                _consoleUI.OutputLine(ConsoleConfig.ERROR_NO_COMMAND, true);
                return;
            }

            object reflectedObject = _container.TryResolve(method.ReflectedType);

            if (reflectedObject == null)
            {
                _consoleUI.OutputLine(ConsoleConfig.ERROR_NO_REFLECTED_OBJECT, true);
                return;
            }

            try
            {
                object retVal = method.Invoke(reflectedObject, parameters);

                if (retVal != null)
                {
                    _consoleUI.OutputLine(retVal.ToString(), true);
                }
            }
            catch (ArgumentException)
            {
                _consoleUI.OutputLine(ConsoleConfig.ERROR_PARAMETERS_MATCH, true);
            }
            catch (TargetParameterCountException)
            {
                _consoleUI.OutputLine(ConsoleConfig.ERROR_PARAMETERS_NUMBER, true);
            }
        }

        private object[] HandleParameters(object[] parameters)
        {
            object[] newParameters = parameters;

            for (int i = 0; i < parameters.Length; i++)
            {
                object parameter = parameters[i];

                if (bool.TryParse(parameter.ToString(), out bool boolValue))
                {
                    newParameters[i] = boolValue;
                }
                else if (float.TryParse(parameter.ToString(), out float floatValue))
                {
                    newParameters[i] = floatValue;
                }
            }

            return newParameters;
        }

        private void OutputDebugMessage(string message, string stack, LogType logType)
        {
            if (!IsInitialized)
            {
                return;
            }
            
            if (!ConsoleConfig.SHOWN_LOG_TYPES.Contains(logType))
            {
                return;
            }
            
            _consoleUI.OutputLine(message + "\n" + stack);
        }

        private void LoadCommands()
        {
            TextAsset json = Resources.Load<TextAsset>(ConsoleConfig.COMMANDS_FILENAME);

            if (json == null)
            {
                Logger.Log($"Failed to load console - can't find commands file.", "Console", Logging.LogType.Error);
                
                return;
            }

            _data = JsonConvert.DeserializeObject<List<ConsoleCommandData>>(json.text);

            foreach (ConsoleCommandData data in _data)
            {
                Type type = Type.GetType(data.Type);

                if (type == null)
                {
                    continue;
                }

                MethodInfo method = type.GetMethod(data.Method);

                if (method == null)
                {
                    continue;
                }

                _commands.Add(data.Command.ToCamelCase(), method);
            }
        }
    }
}
