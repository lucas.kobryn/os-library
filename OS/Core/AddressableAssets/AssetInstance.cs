﻿using System.Collections.Generic;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace OS.Core.AddressableAssets 
{
	/// <summary>
	/// Container class for whenever you need to load an asset using Addressables. This will help to keep just one
	/// instance of an asset and handle when it should be released from memory.
	/// </summary>
	public class AssetInstance
	{
		/// <summary>
		/// Address of the asset.
		/// </summary>
		public string Address { get; private set; }

		/// <summary>
		/// How many objects are still using this asset.
		/// </summary>
		public int RefCount => Subscribers.Count;
		
		/// <summary>
		/// Hashes of objects using this asset.
		/// </summary>
		public List<int> Subscribers { get; private set; } 
		
		/// <summary>
		/// AsyncOperationHandle of this asset.
		/// </summary>
		public AsyncOperationHandle Handle { get; private set; }

		public AssetInstance(string address, AsyncOperationHandle handle)
		{
			Address = address;
			Handle = handle;
			Subscribers = new List<int>();
		}

		public void AddSubscriber(int subscriberHash)
		{
			Subscribers.Add(subscriberHash);
		}
	}
}
