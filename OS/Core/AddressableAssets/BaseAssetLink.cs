﻿using System;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace OS.Core.AddressableAssets 
{
    /// <summary>
    /// Base class that loads a single object of type <see cref="TType"/> using <see cref="IAssetsManager"/>.
    /// </summary>
    /// <typeparam name="TType"></typeparam>
    public class BaseAssetLink<TType> : AssetLink<TType, TType> where TType : UnityEngine.Object
    {
        public BaseAssetLink(IAssetsManager assetsManager, object owner, string address) : base(assetsManager, owner, address) { }
        
        protected override void OnLoadingFailed(Action<TType> onLoaded) { }

        protected override void OnLoadingSuccessful(AsyncOperationHandle<TType> handle, Action<TType> onLoaded)
        {
            onLoaded.Invoke(handle.Result);
        }
    }
}
