namespace OS.Core
{
    public static class ModulePriorities
    {
        public const int CONSOLE_MODULE = -2;
        public const int ASSETS_MODULE = -1;
        public const int PROFILES_SERVICE = 0;
        public const int PROFILE_MANAGER = 1;
        public const int CONTENT_MODULE = 2;
    }
}
