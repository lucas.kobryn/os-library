﻿using UnityEditor;
using UnityEngine;

namespace OS.Core.AddressableAssets.Editor 
{
    [CustomPropertyDrawer(typeof(AddressableAssetAttribute))]
    public class AddressableAssetAttributeDrawer : PropertyDrawer
    {
        private GUIStyle _style;
        private Object _referencedObject;
        private bool _isValid;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            _style = new GUIStyle {alignment = TextAnchor.MiddleRight};
            
            EditorGUI.BeginProperty(position, label, property);
            
            AddressableAssetAttribute addressableAsset = (AddressableAssetAttribute) attribute;

            _isValid = false;
            
            if (property.propertyType == SerializedPropertyType.String)
            {
                position.height = EditorGUIUtility.singleLineHeight;
                
                _referencedObject = EditorGUI.ObjectField(position, new GUIContent(property.displayName), AssetDatabase.LoadAssetAtPath<Object>(property.stringValue), typeof(Object), false);

                if (_referencedObject)
                {
                    string filePath = AssetDatabase.GetAssetPath(_referencedObject);
                    _isValid = filePath.Contains(AddressableAssetsEditorConstants.CONTENT_LOCATION);

                    property.stringValue = _isValid ? filePath : "";
                }
                else
                {
                    property.stringValue = "";
                }
            }
            
            if (!_isValid)
            {
                position.y += EditorGUIUtility.singleLineHeight + AddressableAssetsEditorConstants.SPACING;
                position.height = EditorGUIUtility.singleLineHeight * 2f;

                EditorGUI.HelpBox(position, AddressableAssetsEditorConstants.ADDRESSABLE_ASSET_ATTRIBUTE_INFO_BOX + AddressableAssetsEditorConstants.CONTENT_LOCATION, MessageType.Warning);
            }

            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) + (!_isValid 
                                                                  ? EditorGUIUtility.singleLineHeight * 2f + AddressableAssetsEditorConstants.SPACING 
                                                                  : 0f);
        }
    }
}
