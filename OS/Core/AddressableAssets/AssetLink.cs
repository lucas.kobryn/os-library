﻿using System;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace OS.Core.AddressableAssets 
{
    [Serializable]
    public abstract class AssetLink<TType, TAction> : IAssetLink<TType, TAction> where TType : UnityEngine.Object where TAction : UnityEngine.Object
    {
        public AsyncOperationHandle<TType> Handle { get; protected set; }
        
        public int OwnerHash { get; protected set; }
        public string OwnerName { get; protected set; }
        
        public string Address { get; protected set; }

        public bool IsLoading => Handle.IsValid() && !Handle.IsDone && Handle.Status == AsyncOperationStatus.None;
        public bool IsValid => Handle.IsValid() && Handle.Status != AsyncOperationStatus.Failed;
        public bool IsReleased { get; protected set; }

        /// <summary>
        /// AssetsManager to use for loading and releasing assets.
        /// </summary>
        private readonly IAssetsManager _assetsManager;
        
        protected AssetLink(IAssetsManager assetsManager, object owner, string address)
        {
            _assetsManager = assetsManager;

            OwnerHash = owner.GetHashCode();
            OwnerName = owner.ToString();
            
            Address = address;
        }
        
        public void Load(Action<TAction> onLoaded)
        {
            IsReleased = false;

            if (IsLoading)
            {
                return;
            }

            if (!IsValid)
            {
                Handle = _assetsManager.LoadAsset(this);
            }

            if (Handle.IsDone)
            {
                OnLoadingFinished(Handle, onLoaded);
                return;
            }

            Handle.Completed += (handle) => OnLoadingFinished(handle, onLoaded);
        }

        public void Release()
        {
            IsReleased = true;
            
            _assetsManager.ReleaseAsset(this);
        }

        protected void OnLoadingFinished(AsyncOperationHandle<TType> handle, Action<TAction> onLoaded)
        {
            if (IsReleased)
            {
                return;
            }

            if (!handle.IsValid() || handle.Status == AsyncOperationStatus.Failed)
            {
                OnLoadingFailed(onLoaded);
                return;
            }

            OnLoadingSuccessful(handle, onLoaded);
        }

        protected abstract void OnLoadingFailed(Action<TAction> onLoaded);

        protected abstract void OnLoadingSuccessful(AsyncOperationHandle<TType> handle, Action<TAction> onLoaded);
    }
}
