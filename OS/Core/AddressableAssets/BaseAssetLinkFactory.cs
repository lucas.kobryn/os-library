﻿using Zenject;

namespace OS.Core.AddressableAssets 
{
    /// <summary>
    /// Creates <see cref="BaseAssetLink{TType}"/>.
    /// </summary>
    public class BaseAssetLinkFactory
    {
        private readonly IAssetsManager _assetsManager;

        public BaseAssetLinkFactory(IAssetsManager assetsManager)
        {
            _assetsManager = assetsManager;
        }

        public BaseAssetLink<T> Create<T>(object owner, string address) where T : UnityEngine.Object
        {
            return new BaseAssetLink<T>(_assetsManager, owner, address);
        }
    }
}
