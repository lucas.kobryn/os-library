﻿using System;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace OS.Core.AddressableAssets
{
    public interface IAssetLink<TType, out TAction> where TType : UnityEngine.Object
    {
        /// <summary>
        /// Operation handle of this asset.
        /// </summary>
        AsyncOperationHandle<TType> Handle { get; }
        
        /// <summary>
        /// Link's owner's hash.
        /// </summary>
        int OwnerHash { get; }
        /// <summary>
        /// Link's owner's name.
        /// </summary>
        string OwnerName { get; }

        /// <summary>
        /// Address of an asset to use.
        /// </summary>
        string Address { get; }

        /// <summary>
        /// Determines whether this link is still loading.
        /// </summary>
        bool IsLoading { get; }
        /// <summary>
        /// Determines if this link is valid.
        /// </summary>
        bool IsValid { get; }
        /// <summary>
        /// Determines if this link has already been released.
        /// </summary>
        bool IsReleased { get; }

        /// <summary>
        /// Tries to load this asset.
        /// </summary>
        /// <param name="onLoaded">Action to be called when this asset is loaded.</param>
        void Load(Action<TAction> onLoaded);
        /// <summary>
        /// Releases this link.
        /// </summary>
        void Release();
    }
}
