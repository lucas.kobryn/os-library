﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using OS.Utilities.Coroutines;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace OS.Core.Console
{
    /// <summary>
    /// Displays <see cref="ConsoleModule"/> UI.
    /// </summary>
    public class ConsoleUI : MonoBehaviour
    {
        public bool IsEnabled => _canvas.enabled;
        
        public bool IsInitialized { get; private set; }

        public Action<string> OnSubmit;

        [SerializeField] private Canvas _canvas;
        [SerializeField] private TextMeshProUGUI _outputText;
        [SerializeField] private TMP_InputField _inputField;
        [SerializeField] private Button _closeButton;
        [SerializeField] private ScrollRect _scrollRect;

        private StringBuilder _builder;

        private readonly List<string> _inputs = new List<string>();
        
        private int _inputHistoryPointer = 0;

        public void ClearOutput()
        {
            if (!IsInitialized)
            {
                return;
            }
            
            _builder.Clear();
            _outputText.text = "";
        }

        public void Toggle(bool isOn)
        {
            if (!IsInitialized)
            {
                return;
            }
            
            if (isOn)
            {
                _inputField.ActivateInputField();
            }
            else
            {
                _inputField.text = "";
                _inputField.DeactivateInputField(true);
            }
            
            _canvas.enabled = isOn;
        }

        public void CycleHistory(int direction)
        {
            if (!IsInitialized)
            {
                return;
            }
            
            _inputHistoryPointer = Mathf.Clamp(_inputHistoryPointer + direction, -1, _inputs.Count - 1);
            _inputField.text = _inputHistoryPointer < 0 ? "" : _inputs[_inputHistoryPointer];
        }

        public void OutputLine(string input, bool rich = false)
        {
            if (!IsInitialized || _builder == null)
            {
                return;
            }
            
            if (_builder.Capacity < _builder.Length + input.Length)
            {
                _builder.Clear();
            }

            _builder.Append(ConsoleConfig.TIME_START);
            _builder.Append(DateTime.Now.ToShortTimeString());
            _builder.Append(ConsoleConfig.TIME_END);
            _builder.Append(ConsoleConfig.PREFIX);

            if (!rich)
            {
                _builder.Append(ConsoleConfig.INPUT_START);
            }

            _builder.Append(input);

            if (!rich)
            {
                _builder.Append(ConsoleConfig.INPUT_END);
            }

            _builder.AppendLine();
            
            _outputText.SetText(_builder.ToString());

            LayoutRebuilder.ForceRebuildLayoutImmediate(_scrollRect.content);

            CoroutineRunner.RunCoroutine(ScrollToBottom());
        }
        
        private void Start()
        {
            _builder = new StringBuilder(ConsoleConfig.CHARACTER_LIMIT);
            _closeButton.onClick.AddListener(() => Toggle(false));
            _inputField.onSubmit.AddListener(SubmitInput);

            IsInitialized = true;
        }

        private void OnDestroy()
        {
            if (!IsInitialized)
            {
                return;
            }
            
            _closeButton.onClick.RemoveAllListeners();
            _builder.Clear();
            _builder = null;

            IsInitialized = true;
        }

        private void SubmitInput(string input)
        {
            _inputField.text = "";
            _inputField.ActivateInputField();

            OutputLine(input);

            AddToHistory(input);
            _inputHistoryPointer = _inputs.Count;

            OnSubmit?.Invoke(input);
        }

        private void AddToHistory(string input)
        {
            _inputs.Add(input);
            
            if (_inputs.Count > ConsoleConfig.INPUTS_QUEUE_CAPACITY)
            {
                _inputs.RemoveAt(0);
            }
        }

        private IEnumerator ScrollToBottom()
        {
            yield return new WaitForEndOfFrame();

            _scrollRect.verticalNormalizedPosition = 0f;
        }
    }
}
