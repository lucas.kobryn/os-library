﻿using System;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Logger = OS.Core.Logging.Logger;
using LogType = OS.Core.Logging.LogType;
using Object = UnityEngine.Object;

namespace OS.Core.AddressableAssets 
{
    /// <summary>
    /// Wrapper which simplifies the usage of Addressables package.
    /// </summary>
    public class AssetsManager : IAssetsManager
    {
        public bool IsBusy { get; private set; }

        /// <summary>
        /// Map of assets that are currently loaded - key is an address of an asset, value is an instances.
        /// </summary>
        public readonly Dictionary<string, AssetInstance> _loadedAssets = new Dictionary<string, AssetInstance>();
        /// <summary>
        /// Map of assets that are currently loading - key is an address of an asset, value is an instances.
        /// </summary>
        public readonly Dictionary<string, AssetInstance> _loadingAssets = new Dictionary<string, AssetInstance>();

        public AsyncOperationHandle<TType> LoadAsset<TType, TAction>(IAssetLink<TType, TAction> link) where TType : Object where TAction : Object
        {
            Logger.Log($"{link.OwnerName} {link.OwnerHash} is trying to load {link.Address}.", "Assets Manager");
            AsyncOperationHandle<TType> handle = new AsyncOperationHandle<TType>();

            if (string.IsNullOrEmpty(link.Address))
            {
                Logger.Log("You can not load AssetLinks with null or empty address!", "Assets Manager", LogType.Warning);
                return handle;
            }
            
            if (!_loadedAssets.TryGetValue(link.Address, out AssetInstance loadedAsset))
            {
                Logger.Log("Couldn't find existing instances - making a new one.", "Assets Manager");
                handle = Addressables.LoadAssetAsync<TType>(link.Address);
                
                AddHandle(link.Address, handle, link.OwnerHash);

                handle.Completed += (tmpHandle) => OnLoadingFinished(link.Address, tmpHandle);
            }
            else
            {
                Logger.Log("Found existing instance - trying to use it!", "Assets Manager");
                
                if (loadedAsset.Subscribers.Contains(link.OwnerHash))
                {
                    throw new Exception($"{link.OwnerName} is trying to load {link.Address} but it did not release previous handle!");
                }

                handle = loadedAsset.Handle.Convert<TType>();
                
                loadedAsset.Subscribers.Add(link.OwnerHash);
            }

            return handle;
        }

        public void ReleaseAsset<TType, TAction>(IAssetLink<TType, TAction> link) where TType : Object where TAction : Object
        {
            if (!_loadedAssets.TryGetValue(link.Address, out AssetInstance assetInstance))
            {
                Logger.Log($"{link.Address} could not be found in loaded assets!", "Assets Manager", LogType.Error);
                return;
            }

            assetInstance.Subscribers.Remove(link.OwnerHash);

            if (assetInstance.RefCount > 0)
            {
                return;
            }

            RemoveHandle(assetInstance.Address);
            Addressables.Release(assetInstance.Handle);
        }

        private void OnLoadingFinished<TType>(string address, AsyncOperationHandle<TType> handle)
        {
            _loadingAssets.Remove(address);

            if (handle.Status == AsyncOperationStatus.Succeeded)
            {
                return;
            }

            Logger.Log($"Failed to load {address}!", "Assets Manager", LogType.Error);
            RemoveHandle(address);
        }
        
        private void AddHandle<TType>(string address, AsyncOperationHandle<TType> handle, int subscriberHash)
        {
            AssetInstance assetInstance = new AssetInstance(address, handle);
            assetInstance.AddSubscriber(subscriberHash);

            _loadedAssets.Add(address, assetInstance);
            _loadingAssets.Add(address, assetInstance);
        }

        private void RemoveHandle(string address)
        {
            _loadedAssets.Remove(address);

            Logger.Log($"{address} released!", "Assets Manager");
        }
    }
}
