﻿using System;
using UnityEngine;
using Zenject;

namespace OS.Core.Console
{
    [CreateAssetMenu(fileName = "Inst_Console", menuName = ConsoleConfig.INSTALLERS_PATH + "Console Installer")]
    public class ConsoleInstaller : ScriptableInstaller
    {
        [SerializeField] private ConsoleUI _prefab;
        
        protected override void HandleBindings()
        {
            Container.Bind<ConsoleUI>().FromComponentInNewPrefab(_prefab).WithGameObjectName(_prefab.name).AsSingle().NonLazy();
            Container.Bind<ConsoleModule>().AsSingle().NonLazy();
            Container.Bind<IModule>().WithId(CoreConfig.INITIALIZE_ON_LOAD_ID).To<ConsoleModule>().FromResolve();
            Container.Bind<IDisposable>().To<ConsoleModule>().FromResolve();
            Container.Bind<ITickable>().To<ConsoleModule>().FromResolve();
        }
    }
}
