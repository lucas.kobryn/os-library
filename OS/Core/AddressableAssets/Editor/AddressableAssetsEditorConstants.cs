﻿namespace OS.Core.AddressableAssets.Editor 
{
	public static class AddressableAssetsEditorConstants
	{
		public const string ADDRESSABLE_ASSET_ATTRIBUTE_INFO_BOX = "You must select an object within: ";
		public const string CONTENT_LOCATION = "Assets/";

		public const float SPACING = 2f;
	}
}
