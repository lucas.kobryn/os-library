﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace OS.Core.Console
{
	public static class ConsoleConfig
	{
		public static char[] COMMAND_PARAMETERS_DELIMITERS = {' '};

		public const string ERROR_NO_COMMAND = "<#B00000>Error: Given command does not exist.</color>";
		public const string ERROR_NO_REFLECTED_OBJECT = "<#B00000>Error: Couldn't find suitable object for command invocation.</color>";
		public const string ERROR_PARAMETERS_MATCH = "<#B00000>Error: Wrong parameters given.</color>";
		public const string ERROR_PARAMETERS_NUMBER = "<#B00000>Error: Invalid number of parameters.</color>";
		
		public const string INSTALLERS_PATH = "OS/Core/Installers/";
		public const string COMMANDS_PATH  = "Assets/Resources/";
		public const string COMMANDS_FILENAME = "ConsoleCommands";
		public const string COMMANDS_EXTENSION = "json";

		public const int CHARACTER_LIMIT = 65536;

		public const int INPUTS_QUEUE_CAPACITY = 6; 
		
		public const string TIME_START = "<#9A9A9A>";
		public const string TIME_END = "</color>";
		public const string PREFIX = "<#CD2400> > </color>";
		public const string INPUT_START = "<noparse>";
		public const string INPUT_END = "</noparse>";

		public const bool SHOW_DEBUG_LOGS = true;
		
		public static readonly List<LogType> SHOWN_LOG_TYPES = new List<LogType>{LogType.Log, LogType.Error, LogType.Exception};
	}
}
