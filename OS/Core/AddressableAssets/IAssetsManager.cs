﻿using UnityEngine.ResourceManagement.AsyncOperations;

namespace OS.Core.AddressableAssets 
{
    /// <summary>
    /// Manager interface for loading and unloading assets asynchronously.
    /// </summary>
    public interface IAssetsManager
    {
        /// <summary>
        /// Whenever AssetManager is during an async operation this is set to true.
        /// </summary>
        bool IsBusy { get; }

        /// <summary>
        /// Returns AsyncOperationHandle of an asset that is already loaded or creates and loads a new one if it could not find any.
        /// </summary>
        /// <param name="link">Link of an asset you want to load.</param>
        /// <typeparam name="TType">Type of an asset you want to load.</typeparam>
        /// <typeparam name="TAction">Type of an action to be called when the asset is loaded.</typeparam>
        /// <returns></returns>
        AsyncOperationHandle<TType> LoadAsset<TType, TAction>(IAssetLink<TType, TAction> link) where TType : UnityEngine.Object where TAction : UnityEngine.Object;
        /// <summary>
        /// This will look for an existing asset instance with link's address and remove it's owner from subscribers list.
        /// </summary>
        /// <param name="link">Link to be released.</param>
        /// <typeparam name="TType">Type of an asset you want to release.</typeparam>
        /// <typeparam name="TAction">Type of an action to be called when the asset is loaded.</typeparam>
        void ReleaseAsset<TType, TAction>(IAssetLink<TType, TAction> link) where TType : UnityEngine.Object where TAction : UnityEngine.Object;
    }
}
