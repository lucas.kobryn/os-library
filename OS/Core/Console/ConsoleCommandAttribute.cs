﻿using System;
using UnityEngine;

namespace OS.Core.Console
{
    /// <summary>
    /// Methods marked with this attribute can be invoked during runtime using <see cref="ConsoleModule"/>.
    /// </summary>
    public class ConsoleCommand : Attribute
    {
        public readonly string Name;
        public readonly string Info;

        public ConsoleCommand(string name, string info = "")
        {
            Name = name;
            Info = info;
        }
    }
}
