// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/OS/Core/Console/ConsoleInputActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace OS.Core.Console
{
    public class @ConsoleInputActions : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @ConsoleInputActions()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""ConsoleInputActions"",
    ""maps"": [
        {
            ""name"": ""Console"",
            ""id"": ""7a86c448-8b3d-4f63-bc38-fbbfb4fb0de5"",
            ""actions"": [
                {
                    ""name"": ""Open"",
                    ""type"": ""Button"",
                    ""id"": ""b59913fc-e190-473a-9ec0-a293a3c07f3f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Close"",
                    ""type"": ""Button"",
                    ""id"": ""7d10ac0b-b8fb-452b-a33b-4422a70ea081"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""CycleHistoryUp"",
                    ""type"": ""Button"",
                    ""id"": ""08249cc1-a304-4ac8-821b-56f79fbd394c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""CycleHistoryDown"",
                    ""type"": ""Button"",
                    ""id"": ""d55e39ef-56fb-425d-98be-205a6ad96fba"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""b585e1d9-74f1-4559-a4e0-1b56e17bf376"",
                    ""path"": ""<Keyboard>/backquote"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Open"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a692711f-de5e-4d67-970a-dec2d0258874"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Close"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""10251409-eec2-4d1e-bfd4-a17fb53ed658"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CycleHistoryUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fb1eb05c-91a0-46b9-8e64-49471a43422c"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""CycleHistoryDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
            // Console
            m_Console = asset.FindActionMap("Console", throwIfNotFound: true);
            m_Console_Open = m_Console.FindAction("Open", throwIfNotFound: true);
            m_Console_Close = m_Console.FindAction("Close", throwIfNotFound: true);
            m_Console_CycleHistoryUp = m_Console.FindAction("CycleHistoryUp", throwIfNotFound: true);
            m_Console_CycleHistoryDown = m_Console.FindAction("CycleHistoryDown", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Console
        private readonly InputActionMap m_Console;
        private IConsoleActions m_ConsoleActionsCallbackInterface;
        private readonly InputAction m_Console_Open;
        private readonly InputAction m_Console_Close;
        private readonly InputAction m_Console_CycleHistoryUp;
        private readonly InputAction m_Console_CycleHistoryDown;
        public struct ConsoleActions
        {
            private @ConsoleInputActions m_Wrapper;
            public ConsoleActions(@ConsoleInputActions wrapper) { m_Wrapper = wrapper; }
            public InputAction @Open => m_Wrapper.m_Console_Open;
            public InputAction @Close => m_Wrapper.m_Console_Close;
            public InputAction @CycleHistoryUp => m_Wrapper.m_Console_CycleHistoryUp;
            public InputAction @CycleHistoryDown => m_Wrapper.m_Console_CycleHistoryDown;
            public InputActionMap Get() { return m_Wrapper.m_Console; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(ConsoleActions set) { return set.Get(); }
            public void SetCallbacks(IConsoleActions instance)
            {
                if (m_Wrapper.m_ConsoleActionsCallbackInterface != null)
                {
                    @Open.started -= m_Wrapper.m_ConsoleActionsCallbackInterface.OnOpen;
                    @Open.performed -= m_Wrapper.m_ConsoleActionsCallbackInterface.OnOpen;
                    @Open.canceled -= m_Wrapper.m_ConsoleActionsCallbackInterface.OnOpen;
                    @Close.started -= m_Wrapper.m_ConsoleActionsCallbackInterface.OnClose;
                    @Close.performed -= m_Wrapper.m_ConsoleActionsCallbackInterface.OnClose;
                    @Close.canceled -= m_Wrapper.m_ConsoleActionsCallbackInterface.OnClose;
                    @CycleHistoryUp.started -= m_Wrapper.m_ConsoleActionsCallbackInterface.OnCycleHistoryUp;
                    @CycleHistoryUp.performed -= m_Wrapper.m_ConsoleActionsCallbackInterface.OnCycleHistoryUp;
                    @CycleHistoryUp.canceled -= m_Wrapper.m_ConsoleActionsCallbackInterface.OnCycleHistoryUp;
                    @CycleHistoryDown.started -= m_Wrapper.m_ConsoleActionsCallbackInterface.OnCycleHistoryDown;
                    @CycleHistoryDown.performed -= m_Wrapper.m_ConsoleActionsCallbackInterface.OnCycleHistoryDown;
                    @CycleHistoryDown.canceled -= m_Wrapper.m_ConsoleActionsCallbackInterface.OnCycleHistoryDown;
                }
                m_Wrapper.m_ConsoleActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Open.started += instance.OnOpen;
                    @Open.performed += instance.OnOpen;
                    @Open.canceled += instance.OnOpen;
                    @Close.started += instance.OnClose;
                    @Close.performed += instance.OnClose;
                    @Close.canceled += instance.OnClose;
                    @CycleHistoryUp.started += instance.OnCycleHistoryUp;
                    @CycleHistoryUp.performed += instance.OnCycleHistoryUp;
                    @CycleHistoryUp.canceled += instance.OnCycleHistoryUp;
                    @CycleHistoryDown.started += instance.OnCycleHistoryDown;
                    @CycleHistoryDown.performed += instance.OnCycleHistoryDown;
                    @CycleHistoryDown.canceled += instance.OnCycleHistoryDown;
                }
            }
        }
        public ConsoleActions @Console => new ConsoleActions(this);
        public interface IConsoleActions
        {
            void OnOpen(InputAction.CallbackContext context);
            void OnClose(InputAction.CallbackContext context);
            void OnCycleHistoryUp(InputAction.CallbackContext context);
            void OnCycleHistoryDown(InputAction.CallbackContext context);
        }
    }
}
