﻿using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;
using Logger = OS.Core.Logging.Logger;
using LogType = OS.Core.Logging.LogType;

namespace OS.Core.AddressableAssets 
{
    /// <summary>
    /// Initializes Addressables.
    /// </summary>
    public class AssetsModule : IModule
    {
        public bool IsInitialized { get; private set; }

        public string Name => "Assets Module";
        
        public int Priority => ModulePriorities.ASSETS_MODULE;

        public void Initialize()
        {
            Addressables.InitializeAsync().Completed += OnInitialized;
        }

        private void OnInitialized(AsyncOperationHandle<IResourceLocator> handle)
        {
            if (handle.Status != AsyncOperationStatus.Succeeded)
            {
                Debug.LogError("Could not initialize Addressables!");
                return;
            }

            Logger.Log($"Successfully initialized {Name}!", Name);

            IsInitialized = true;
        }

        public void CleanUp() { }
    }
}
